## Setup step

1. Using https git clone https://mitesh_dev@bitbucket.org/mitesh_dev/biztech_practical.git where your apache is setup E.G public_html  
2. After clone you will see biztech_practical folder in our directory.
3. After clone create database and dump sql query from query.sql file which you can find in clone directory.
4. Next you can change your database connection configuration from config.php in clone directory..
5. After above all step done just run project in browser and enjoy it :)

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://bitbucket.org/mitesh_dev/biztech_practical/src/master/).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).