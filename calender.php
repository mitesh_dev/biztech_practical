<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calendar | Biztech IT Consultancy Pvt Ltd.</title>
    <link rel="stylesheet" href="assets/css/calender.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
<header>
    <div class="title-content">
      <button id="last"></button><span>
      <h1 id="month"></h1>
      <h2 id="year"></h2></span>
        <button id="next"></button>
    </div>
</header>
<div class="ci-cal-container" id="calendar"></div>
<script  src="assets/js/calender.js"></script>
</body>
</html>