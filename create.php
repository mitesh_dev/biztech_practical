<?php include_once 'header.php'; ?>
<?php
$fullNameError = $emailIdError = $phoneNumberError = $departmentError = $joiningDateError = '';
$error = false;
if ( isset($_POST['submit']) ) {

    $fullName    = trim($_POST['fullName']);
    $emailId     = trim($_POST['emailId']);
    $phoneNumber = trim($_POST['phoneNumber']);
    $department  = trim($_POST['department']);
    $joiningDate = trim($_POST['joiningDate']);
    
    if (empty($fullName)) {
        $error = true;
        $fullNameError = "Please enter your full name.";
    }  else if (empty($emailId) || !filter_var($emailId,FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $emailIdError = "Please enter valid email address.";
    } else if (empty($phoneNumber) || strlen($phoneNumber) < 10) {
        $error = true;
        $phoneNumberError = "Please enter valid number.";
    } else if (empty($department)) {
        $error = true;
        $departmentError = "Please enter your department.";
    } else if (empty($joiningDate)) {
        $error = true;
        $joiningDateError = "Please enter your joining date.";
    }

    if( !$error ) {

        if ( isset($_GET['id']) && !empty($_GET['id']) ) {
            $id = $_GET['id'];
            $query = "UPDATE employee SET fullName = '$fullName', emailId = '$emailId', phoneNumber = '$phoneNumber', department = '$department', joiningDate = '$joiningDate' WHERE id='$id'";
        } else {
            $query = "INSERT INTO employee(fullName, emailId, phoneNumber, department, joiningDate) 
                  VALUES('".$fullName ."','".$emailId."','".$phoneNumber."','".$department."','".$joiningDate."')";
        }

        $response = $mysqli->query($query);

        if ($response) {
            $uniqueId = strtotime("now");
            $filename = $fullName.'_'.$uniqueId.'.csv';
            ob_clean();
            ob_start();
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header('Pragma: no-cache');
            header('Expires: 0');
            $file = fopen('php://output', 'w');
            fputcsv($file, ['fullName', 'emailId', 'phoneNumber', 'department', 'joiningDate']);

            $data = [[ $fullName, $emailId, $phoneNumber, $department, $joiningDate ]];
            foreach ($data as $row) {
                fputcsv($file, $row);
            }
            fclose($file);
            $errType = "success";
            $errMSG = "operation successfully!!";
            exit();
        } else {
            $errType = "danger";
            $errMSG = "Something went wrong, try again later...";
        }
    }
}

$action = htmlspecialchars($_SERVER['PHP_SELF']);
if ( isset($_GET['id']) && !empty($_GET['id']) ) {
    $id = $_GET["id"];
    $action = htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$id;
    $query = "SELECT * FROM employee WHERE id='".$id."'";
    $result = mysqli_query($mysqli, $query);
    $row = mysqli_fetch_assoc($result);
}


?>
<div class="page-header clearfix">
    <h2 class="pull-left">Back</h2>
    <a href="employee.php" class="btn btn-success pull-right">Employee List</a>
</div>

<?php
if ( isset($errMSG) ) : ?>
    <div class="form-group">
        <div class="alert alert-<?php echo ($errType=="success") ? "success" : $errType; ?>">
            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
        </div>
    </div>
<?php endif; ?>

<form class="form-horizontal" method="post" action="<?php echo $action ; ?>">
    <div class="form-group">
        <label for="exampleInputName">Name</label>
        <input type="text" name="fullName" value="<?php echo (isset($_GET['id'])) ? $row['fullName'] : '' ?>" class="form-control" id="exampleInputName" aria-describedby="nameHelp" placeholder="Enter Name" required>
        <span class="text-danger"><?php echo $fullNameError; ?></span>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="emailId" value="<?php echo (isset($_GET['id'])) ? $row['emailId'] : '' ?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
        <span class="text-danger"><?php echo $emailIdError; ?></span>
    </div>
    <div class="form-group">
        <label for="exampleInputNumber">Mobile</label>
        <input type="text" name="phoneNumber" value="<?php echo (isset($_GET['id'])) ? $row['phoneNumber'] : '' ?>" class="form-control" id="exampleInputNumber" aria-describedby="numberHelp" placeholder="Enter Mobile" required>
        <span class="text-danger"><?php echo $phoneNumberError; ?></span>
    </div>
    <div class="form-group">
        <label for="exampleInputDepartment">Department</label>
        <input type="text" name="department" value="<?php echo (isset($_GET['id'])) ? $row['department'] : '' ?>" class="form-control" id="exampleInputDepartment" aria-describedby="departmentHelp" placeholder="Enter Department" required>
        <span class="text-danger"><?php echo $departmentError; ?></span>
    </div>
    <div class="form-group">
        <label for="exampleInputJoiningDate">Joining Date</label>
        <input type="date" name="joiningDate" value="<?php echo (isset($_GET['id'])) ? $row['joiningDate'] : '' ?>" class="form-control" id="exampleInputJoiningDate" aria-describedby="JoiningDateHelp" placeholder="Enter Joining Date" required>
        <span class="text-danger"><?php echo $joiningDateError; ?></span>
    </div>

    <button type="submit" name="submit" class="btn btn-primary">Submit</button>

</form>
<?php include_once 'footer.php'; ?>
