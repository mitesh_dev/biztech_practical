<?php include_once 'header.php';

if(isset($_POST["import"])){

    $filename=$_FILES["export_file"]["tmp_name"];
    if($_FILES["export_file"]["size"] > 0)
    {
        $file = fopen($filename, "r");
        while (($getData = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            $fullName = trim($getData[0]);
            $emailId = trim($getData[1]);
            $phoneNumber = trim($getData[2]);
            $department = trim($getData[3]);
            $joiningDate = trim($getData[4]);

            if (!empty($fullName)
                && !empty($emailId)
                && !empty($phoneNumber)
                && !empty($department)
                && !empty($joiningDate)
            ) {
                $query = "INSERT into employee(fullName,emailId,phoneNumber,department,joiningDate) 
                      VALUES ('".$fullName."','".$emailId."','".$phoneNumber."','".$department."','".$joiningDate."')";
                $result = $mysqli->query($query);

                if(!isset($result)) {
                    $errType = "danger";
                    $errMSG = "Something went wrong, try again later...";
                } else {
                    $errType = "success";
                    $errMSG = "employee data import successfully!!";
                }
            } else {
                $errType = "danger";
                $errMSG = "Something went wrong, try again later...";
            }
        }

        fclose($file);
    }
}


?>

<div class="page-header clearfix">
    <h2 class="pull-left">Back</h2>
    <a href="employee.php" class="btn btn-success pull-right">Employee List</a>
</div>
<?php
if( isset($errMSG) ) : ?>
    <div class="form-group">
        <div class="alert alert-<?php echo ($errType=="success") ? "success" : $errType; ?>">
            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
        </div>
    </div>
<?php endif; ?>
<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label" for="filebutton">Select File</label>
        <input type="file" name="export_file" id="filebutton" class="input-large" required/>
    </div>

    <div class="form-group">
        <button type="submit" name="import" class="btn btn-primary">Submit</button>
    </div>
</form>
<?php include_once 'footer.php'; ?>
