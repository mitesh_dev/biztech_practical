<?php include_once 'header.php'; ?>
<div class="page-header clearfix">
    <h2 class="pull-left">Employee Details</h2>
    <a href="create.php" class="btn btn-success pull-right">Add New Employee</a>
</div>
<?php
// Attempt select query execution
$sql = "SELECT * FROM employee";
if($result = $mysqli->query($sql)){
    if($result->num_rows > 0){
        echo "<table class='table table-bordered table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>#</th>";
        echo "<th>Name</th>";
        echo "<th>Email</th>";
        echo "<th>Phone Number</th>";
        echo "<th>Department</th>";
        echo "<th>Joining Date</th>";
        echo "<th>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        while($row = $result->fetch_array()){
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['fullName'] . "</td>";
            echo "<td>" . $row['emailId'] . "</td>";
            echo "<td>" . $row['phoneNumber'] . "</td>";
            echo "<td>" . $row['department'] . "</td>";
            echo "<td>" . $row['joiningDate'] . "</td>";
            echo "<td>";
            echo "<a href='create.php?id=". $row['id'] ."' title='Edit Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
            echo "<a href='export.php?id=". $row['id'] ."' title='Export Record' data-toggle='tooltip'><span class='glyphicon glyphicon-export'></span></a>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        // Free result set
        $result->free();
    } else{
        echo "<p class='lead'><em>No records were found!!</em></p>";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . $mysqli->error;
}

// Close connection
$mysqli->close();
?>
<?php include_once 'footer.php'; ?>
